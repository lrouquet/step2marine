private val CATEGORY_COLORS = arrayOf("ae4784", "5fbb48", "6d59ce", "bab22d", "9f3fad", "4dc381", "d847a9", "598528", "cc77e3", "96b34c", "637ae3", "dc9535", "5b81c4", "d8562e", "4ab4dd", "dc3b67", "59c8b5", "b2463d", "38977f", "e97aad", "3c8848", "8059a2", "b4a658", "c494d6", "6d6c28", "963e59", "84b072", "b3667c", "2a6a45", "e68281", "9c6129", "da9567")

fun buildLatex(Nr: Int, step2: Step2) =
    LatexWriter("standalone") {

        var active = ""
        var free = ""
        val PERM = intArrayOf(
            31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10,
            15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26
        )

        val cats = mutableListOf<String>()

        preamble {
            usePackage(tikz)
            usePackage(xColor) {
                active = color("active", "757575")
                free = color("free", "424242")

                for ((i, color) in CATEGORY_COLORS.withIndex()) {
                    cats += color("cat$i", color)
                }
            }
        }

        document {
            fun color(v: Int) = when (v) {
                0 -> "white"
                -1 -> free
                else -> active
            }

            tikzPicture(scale = .8, style = "thick") {
                fun input(x: Int, y: Int, lValue: Int, rValue: Int, ctx: TikzScope.() -> Unit = {}) {
                    fun _input(value: Int) {
                        val code = if (value < 0) "F" else value.toString()
                        val color = if (value == 0) "black" else "white"

                        draw("fill=${color(value)}") {
                            +Point(0, 0); +Point(1, 0); +Point(0, 1); +Point(-1, 0); -Cycle
                        }
                        node(code, Point(.5, .5), color)
                    }

                    scope(x * 6, -(y * 6)) {
                        _input(lValue)
                        scope(3, 0) {
                            _input(rValue)
                        }

                        ctx()
                    }
                }

                fun block(x: Int, y: Int, lValue: Int, rValue: Int) {
                    input(x, y, lValue, rValue) {
                        val sbColor = if (lValue != 0) "gray" else "white"
                        node(
                            "\\Large S",
                            Point(2, -1),
                            "draw=black, fill=$sbColor, minimum height=1.2cm, minimum width=1.2cm"
                        )
                        draw("->") { +Point(.5, -1); +Point(.75, 0) }
                        draw("<-") { +Point(3.5, -1); +Point(-.75, 0) }
                        draw { +Point(.5, 0); +Point(0, -2); }
                        draw { +Point(3.5, 0); +Point(0, -2); }
                    }
                }

                fun wires(x: Int, y: Int, xTo: Int, yTo: Int, color: String = "black", style: String = "") {
                    val xOffset = if ((x % 2) == 0) .5 else 3.5
                    val xToOffset = if ((xTo % 2) == 0) .5 else 3.5
                    draw("$color, $style") {
                        +Point(
                            x / 2 * 6 + xOffset,
                            -6 * y - 2
                        ); -Point(xTo / 2 * 6 + xToOffset, -6 * yTo + 1);
                    }
                }

                for (i in 0 until Nr) {
                    for (j in 0..15) {
                        block(j, i, step2.δXupper[i][j * 2].value, step2.δXupper[i][j * 2 + 1].value)
                    }
                }
                for (j in 0..15) {
                    input(j, Nr, step2.δXupper[Nr][j * 2].value, step2.δXupper[Nr][j * 2 + 1].value)
                }
                for (i in 0 until Nr) {
                    for (j in 0..31) {
                        if (step2.δXupper[i + 1][PERM[j]].value != 0) {
                            wires(j, i, PERM[j], i + 1, cats[j], "->")
                        } else {
                            wires(j, i, PERM[j], i + 1, cats[j], "->, densely dashed, opacity=.5")
                        }
                    }
                }

                scope(0, -Nr * 7) {
                    for (i in 0 until Nr) {
                        for (j in 0..15) {
                            block(j, i, step2.δXlower[i][j * 2].value, step2.δXlower[i][j * 2 + 1].value)
                        }
                    }
                    for (j in 0..15) {
                        input(j, Nr, step2.δXlower[Nr][j * 2].value, step2.δXlower[Nr][j * 2 + 1].value)
                    }
                    for (i in 0 until Nr) {
                        for (j in 0..31) {
                            if (step2.δXlower[i + 1][PERM[j]].value != 0) {
                                wires(j, i, PERM[j], i + 1, cats[j], "<-")
                            } else {
                                wires(j, i, PERM[j], i + 1, cats[j], "<-, densely dashed, opacity=.5")
                            }
                        }
                    }
                }
            }
        }
    }

fun buildLatex(Nr: Int, step2: Step1Solution) =
    LatexWriter("standalone") {

        var active = ""
        var free = ""
        val PERM = intArrayOf(
            31, 6, 29, 14, 1, 12, 21, 8, 27, 2, 3, 0, 25, 4, 23, 10,
            15, 22, 13, 30, 17, 28, 5, 24, 11, 18, 19, 16, 9, 20, 7, 26
        )

        val cats = mutableListOf<String>()

        preamble {
            usePackage(tikz)
            usePackage(xColor) {
                active = color("active", "757575")
                free = color("free", "424242")

                for ((i, color) in CATEGORY_COLORS.withIndex()) {
                    cats += color("cat$i", color)
                }
            }
        }

        document {
            fun color(v: Int) = when (v) {
                0 -> "white"
                -1 -> free
                else -> active
            }

            tikzPicture(scale = .8, style = "thick") {
                fun input(x: Int, y: Int, lValue: Int, lFree: Int, rValue: Int, rFree: Int, ctx: TikzScope.() -> Unit = {}) {
                    fun _input(value: Int, free: Int) {
                        val code = if (free == 1) "F" else value.toString()
                        val color = if (value == 0) "black" else "white"

                        draw("fill=${color(value)}") {
                            +Point(0, 0); +Point(1, 0); +Point(0, 1); +Point(
                            -1,
                            0
                        ); -Cycle
                        }
                        node(code, Point(.5, .5), color)
                    }

                    scope(x * 6, -(y * 6)) {
                        _input(lValue, lFree)
                        scope(3, 0) {
                            _input(rValue, rFree)
                        }

                        ctx()
                    }
                }

                fun block(x: Int, y: Int, lValue: Int, lFree: Int, rValue: Int, rFree: Int) {
                    input(x, y, lValue, lFree, rValue, rFree) {
                        val sbColor = if (lValue != 0) "gray" else "white"
                        node(
                            "\\Large S",
                            Point(2, -1),
                            "draw=black, fill=$sbColor, minimum height=1.2cm, minimum width=1.2cm"
                        )
                        draw("->") { +Point(.5, -1); +Point(.75, 0) }
                        draw("<-") { +Point(3.5, -1); +Point(-.75, 0) }
                        draw { +Point(.5, 0); +Point(0, -2); }
                        draw { +Point(3.5, 0); +Point(0, -2); }
                    }
                }

                fun wires(x: Int, y: Int, xTo: Int, yTo: Int, color: String = "black", style: String = "") {
                    val xOffset = if ((x % 2) == 0) .5 else 3.5
                    val xToOffset = if ((xTo % 2) == 0) .5 else 3.5
                    draw("$color, $style") {
                        +Point(
                            x / 2 * 6 + xOffset,
                            -6 * y - 2
                        ); -Point(xTo / 2 * 6 + xToOffset, -6 * yTo + 1);
                    }
                }

                for (i in 0 until Nr) {
                    for (j in 0..15) {
                        block(j, i, step2.ΔXupper[i][j * 2], step2.freeXupper[i][j * 2], step2.ΔXupper[i][j * 2 + 1], step2.freeXupper[i][j * 2 + 1])
                    }
                }
                for (j in 0..15) {
                    input(j, Nr, step2.ΔXupper[Nr][j * 2], step2.freeXupper[Nr][j * 2], step2.ΔXupper[Nr][j * 2 + 1], step2.freeXupper[Nr][j * 2 + 1])
                }
                for (i in 0 until Nr) {
                    for (j in 0..31) {
                        if (step2.ΔXupper[i + 1][PERM[j]] != 0) {
                            wires(j, i, PERM[j], i + 1, cats[j], "->")
                        } else {
                            wires(j, i, PERM[j], i + 1, cats[j], "->, densely dashed, opacity=.5")
                        }
                    }
                }

                scope(0, -Nr * 7) {
                    for (i in 0 until Nr) {
                        for (j in 0..15) {
                            block(j, i, step2.ΔXlower[i][j * 2], step2.freeXlower[i][j * 2], step2.ΔXlower[i][j * 2 + 1], step2.freeXlower[i][j * 2 + 1])
                        }
                    }
                    for (j in 0..15) {
                        input(j, Nr, step2.ΔXlower[Nr][j * 2], step2.freeXlower[Nr][j * 2], step2.ΔXlower[Nr][j * 2 + 1], step2.freeXlower[Nr][j * 2 + 1])
                    }
                    for (i in 0 until Nr) {
                        for (j in 0..31) {
                            if (step2.ΔXlower[i + 1][PERM[j]] != 0) {
                                wires(j, i, PERM[j], i + 1, cats[j], "<-")
                            } else {
                                wires(j, i, PERM[j], i + 1, cats[j], "<-, densely dashed, opacity=.5")
                            }
                        }
                    }
                }
            }
        }
    }
