import com.github.rloic.phd.core.mzn.*
import com.github.rloic.phd.core.utils.FromArgs
import com.github.rloic.phd.core.utils.expectArgument
import com.github.rloic.phd.infra.CommonFznSolver
import java.lang.RuntimeException
import java.lang.StringBuilder

class GurobiBinary(
    private val path: String,
    private val nbThreads: Int = 1
) : MznSolver {

    companion object : FromArgs<GurobiBinary> {
        override fun from(args: Map<String, String>) = GurobiBinary(
            args.expectArgument("MiniZinc")
        )
    }


    override fun optimize(model: MznModel.Optimization, data: Map<String, Any>): MznSolution? {
        val (command, process) = createCommandAndProcess(model, data)

        var prevSolution = StringBuilder()
        var solution = StringBuilder()
        var tmp: StringBuilder

        process.inputStream.bufferedReader().useLines { lines ->
            for (line in lines) {
                if (line.startsWith("---")) {
                    tmp = prevSolution
                    prevSolution = solution
                    solution = tmp
                    solution.clear()
                } else {
                    solution.appendLine(line)
                }
            }
        }

        val returnCode = process.waitFor()
        if (returnCode != 0) throw RuntimeException("Invalid return code $returnCode for command: \n$ $command \n```\n$solution\n```")

        return if (prevSolution.isEmpty()) {
            null
        } else {
            MznSolution(prevSolution.toString())
        }
    }

    override fun solveOnce(model: MznModel.CompleteSearch, data: Map<String, Any>): MznSolution? =
        solve(model, data)

    override fun solveOnce(model: MznModel.PartialSearch, data: Map<String, Any>): MznSolution? =
        solve(model, data)

    private fun buildCommand(model: MznModel, data: Map<String, Any>): List<String> =
        if (data.isEmpty()) {
            listOf(path, "--solver", "gurobi", "-s", "-p", nbThreads.toString(), model.value.absolutePath)
        } else {
            val serializedData = data.entries.joinToString(";") { (key, value) -> "$key=$value" }
            listOf(path, "--solver", "gurobi", "-s", "-p", nbThreads.toString(), model.value.absolutePath, "-D", serializedData)
        }

    private fun createCommandAndProcess(model: MznModel, data: Map<String, Any>): CommonFznSolver.CommandAndProcess {
        val command = buildCommand(model, data)
        val process = ProcessBuilder(command)
            .start()

        return CommonFznSolver.CommandAndProcess(command, process)
    }

    private fun solve(model: MznModel, data: Map<String, Any>): MznSolution? {
        val (command, process) = createCommandAndProcess(model, data)

        val solution = StringBuilder()
        process.inputStream.bufferedReader().useLines { lines ->
            for (line in lines) {
                solution.appendLine(line)
            }
        }

        if (process.waitFor() != 0) throw CommonFznSolver.ExecutionError(command)

        return if (solution.endsWith("=====UNSATISFIABLE=====\n")) {
            null
        } else {
            MznSolution(solution.toString())
        }
    }
}