object Cycle

data class Point(val x: Number, val y: Number) {
    operator fun plus(other: Point) = Point(x.toDouble() + other.x.toDouble(), y.toDouble() + other.y.toDouble())
}

data class Arc(val x: Number, val y: Number, val z: Number)

interface Latex {

    infix fun rawLatex(latex: String)

    val tikz: Tikz.Package
    val xColor: XColor.Package

    fun bold(txt: String) = "\\textbf{$txt}"

    fun preamble(fn: LatexPreamble.() -> Unit = {})
    fun document(fn: LatexDocument.() -> Unit = {})
    fun include(path: String, documentContentOnly: Boolean = true)

}

interface LatexPreamble {

    fun <P: LatexPackage> usePackage(pkgName: P, fn: P.() -> Unit = {})

}

sealed interface LatexPackage {
    val name: String
}

interface Tikz {
    interface Package: Tikz, LatexPackage {
        override val name get() = "tikz"
    }
    fun useTikzLibrary(libName: String)
}

interface XColor {
    interface Package: XColor, LatexPackage {
        override val name get() = "xcolor"
    }
    fun color(name: String, color: String, model: String = "HTML"): String
}

interface FrameContext : LatexDocument

interface LatexDocument {

    fun tikzPicture(style: String? = null, scale: Number = 1, fn: TikzContext.() -> Unit = {})
    fun frame(arguments: String? = null, fn: FrameContext.() -> Unit = {})
    operator fun String.unaryPlus()

    fun section(name: String)
    fun subSection(name: String)
    fun itemize(fn: ItemizeContext.() -> Unit = {})
    fun onSlides(slides: String, fn: LatexDocument.() -> Unit = {})
    fun only(slides: String, fn: LatexDocument.() -> Unit = {})
    fun <T> tabular(
        items: Iterable<T>,
        columns: Cols<T>,
        beforeHeader: String? = null,
        afterHeader: String? = null
    )

    val centering: Unit

}

data class Col<T>(
    val name: String,
    val alignment: String = "c",
    val headerAlignment: String? = null,
    val dataAlignment: String? = null,
    val color: String? = null,
    val headerColor: String? = null,
    val dataColor: String? = null,
    val getter: (T) -> Any?
)
fun <T> Cols(vararg columns: Col<T>) = Cols(columns.toList())
data class Cols<T>(val columns: List<Col<T>>)

interface ItemizeContext {

    operator fun String.unaryPlus()

}

interface TikzScope : TikzContext

interface TikzContext {

    fun node(text: String, at: Point, style: String? = null)
    fun draw(style: String? = null, smooth: Boolean = false, fn: DrawContext.() -> Unit = {})
    fun fill(style: String? = null, smooth: Boolean = false, fn: DrawContext.() -> Unit = {})
    fun scope(x: Number, y: Number, style: String? = null, fn: TikzScope.() -> Unit = {})

}

interface DrawContext {

    operator fun Cycle.unaryMinus();
    operator fun Point.unaryPlus()
    operator fun Point.unaryMinus()

    fun arc(at: Point, x: Number, y: Number, z: Number)

}