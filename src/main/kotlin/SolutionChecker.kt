import java.io.File

private fun getResourceAsText(resourcePath: String): String {
    return object {}.javaClass.getResource(resourcePath)!!.readText()
}

fun writeTo(Nr: Int, gamma: IntArray, delta: IntArray, path: String) {
    val content = getResourceAsText("verif.rs")
        .replace("%NR%", Nr.toString())
        .replace("%GAMMA%", gamma.joinToString(", ") { it.toString() })
        .replace("%DELTA%", delta.joinToString(", ") { it.toString() })
        .replace("-1", "FREE()")

    File(path)
        .writeText(content)
}