import java.io.File
import java.lang.StringBuilder

private data class TikzWrapper<T: Tikz>(val delegate: T): Tikz by delegate, Tikz.Package
private data class XColorWrapper<T: XColor>(val delegate: T): XColor by delegate, XColor.Package

class LatexWriter(className: String, classArgument: String? = null, onInit: Latex.() -> Unit) : Latex, LatexDocument,
    LatexPreamble, TikzContext, DrawContext, TikzScope, FrameContext, ItemizeContext, Tikz, XColor {

    override val tikz: Tikz.Package get() = TikzWrapper(this)
    override val xColor: XColor.Package get() = XColorWrapper(this)

    enum class ContextType {
        Default, Itemize
    }

    private val scales = mutableListOf(1.0)
    private fun scale(x: Number): Double {
        val value = scales[scales.size - 1] * x.toDouble()
        return (value * 1000.0).toInt() / 1000.0
    }
    private val contexts = mutableListOf(ContextType.Default)
    private val context get() = contexts[contexts.size - 1]
    private var firstPoint = true

    private val content = StringBuilder()

    init {
        if (classArgument != null) {
            content.append("\\documentclass[$classArgument]{$className}\n")
        } else {
            content.append("\\documentclass{$className}\n")
        }
        onInit()
    }

    fun writeTo(path: String): File {
        val output = File(path)
        output.writeText(content.toString())
        return output
    }

    fun compile(path: String): File {
        val output = writeTo(path)
        ProcessBuilder("tectonic", output.absolutePath)
            .inheritIO()
            .start()
            .waitFor()
        return File(path.replaceAfterLast('.', "pdf"))
    }

    fun preview(path: String) {
        ProcessBuilder("evince", compile(path).absolutePath)
            .inheritIO()
            .start()
            .waitFor()
    }

    override fun useTikzLibrary(libName: String) {
        content.append("\\usetikzlibrary{$libName}\n")
    }

    override fun rawLatex(latex: String) {
        content.append(latex)
        content.append("\n")
    }

    override fun toString() = content.toString()

    override fun preamble(fn: LatexPreamble.() -> Unit) {
        fn()
    }

    private fun scoped(
        scopeName: String,
        args: String? = null,
        beging: Char = '[',
        end: Char = ']',
        newContext: ContextType? = null,
        fn: () -> Unit
    ) {
        if (args != null) {
            content.append("\\begin{$scopeName}$beging$args$end\n")
        } else {
            content.append("\\begin{$scopeName}\n")
        }
        if (newContext != null) {
            contexts += newContext
            fn()
            contexts.removeAt(contexts.size - 1)
        } else {
            fn()
        }
        content.append("\\end{$scopeName}\n")
    }

    override fun <P : LatexPackage> usePackage(pkgName: P, fn: P.() -> Unit) {
        content.append("\\usepackage{${pkgName.name}}\n")
        pkgName.fn()
    }

    override fun tikzPicture(style: String?, scale: Number, fn: TikzContext.() -> Unit) {
        scales.add(scale.toDouble())
        scoped("tikzpicture", style) { fn() }
        scales.removeAt(scales.size - 1)
    }

    override fun frame(arguments: String?, fn: FrameContext.() -> Unit) {
        scoped("frame", arguments, '{', '}') { fn() }
    }

    override fun document(fn: LatexDocument.() -> Unit) {
        scoped("document") { fn() }
    }

    override fun include(path: String, documentContentOnly: Boolean) {
        var includedContent = File(path).readText()
        if (documentContentOnly) {
            includedContent = includedContent.substringAfter("\\begin{document}\n")
                .substringBeforeLast("\\end{document}\n")
        }
        content.append(includedContent)
        content.append("\n")
    }

    override fun color(name: String, color: String, model: String): String {
        content.append("\\definecolor{$name}{$model}{$color}\n")
        return name
    }

    override fun scope(x: Number, y: Number, style: String?, fn: TikzScope.() -> Unit) {
        if (style != null) {
            scoped("scope", "shift={(${scale(x)} , ${scale(y)})},$style") { fn() }
        } else {
            scoped("scope", "shift={(${scale(x)} , ${scale(y)})}") { fn() }
        }

    }

    override val centering: Unit get() = rawLatex("\\centering")

    override fun <T> tabular(items: Iterable<T>, columns: Cols<T>, beforeHeader: String?, afterHeader: String?) {

        val args = columns.columns.joinToString("") {
            val columnColor = it.dataColor ?: it.color
            val dataAlignment = it.dataAlignment ?: it.alignment

            val alignment = StringBuilder()
            if (columnColor != null) {
                alignment.append(">{\\columncolor{$columnColor}}")
            }
            alignment.append(dataAlignment)

            alignment.toString()
        }

        scoped("tabular", args, '{', '}') {
            content.append("\\hline \n")

            if (beforeHeader != null) {
                content.append(beforeHeader)
                content.append('\n')
            }

            content.append(columns.columns.joinToString(" & ") {
                val overrideBgHeaderColor = it.headerColor
                val overrideAlignment = it.headerAlignment

                val columnHeader = StringBuilder()
                if (overrideBgHeaderColor != null || overrideAlignment != null) {
                    val alignment = overrideAlignment ?: it.alignment
                    val bgColor = overrideBgHeaderColor ?: it.color
                    columnHeader.append("\\multicolumn{1}{")
                    if (bgColor != null) {
                        columnHeader.append(">{\\columncolor{${bgColor}}}")
                    }
                    columnHeader.append(alignment)
                    columnHeader.append("}{")
                }
                columnHeader.append(it.name)
                if (overrideBgHeaderColor != null || overrideAlignment != null) {
                    columnHeader.append('}')
                }

                columnHeader.toString()
            })
            content.append(" \\\\ \\hline")

            if (afterHeader != null) {
                content.append(afterHeader)
                content.append('\n')
            }

            for (item in items) {
                content.append('\n')
                content.append(columns.columns.map { it.getter(item) }.joinToString(" & ") { it.toString() })
                content.append(" \\\\")
            }

            content.append("\n\\hline \n")
        }
    }

    override fun draw(style: String?, smooth: Boolean, fn: DrawContext.() -> Unit) {
        content.append("\\draw")
        if (style != null) content.append("[$style]")
        content.append(' ')
        firstPoint = true
        fn()
        content.append(";\n")
    }

    override fun arc(at: Point, x: Number, y: Number, z: Number) {
        content.append("(${scale(at.x)}, ${scale(at.y)}) arc ($x:$y:${scale(z)})")
    }

    override fun fill(style: String?, smooth: Boolean, fn: DrawContext.() -> Unit) {
        content.append("\\fill")
        if (style != null) content.append("[$style]")
        content.append(' ')
        firstPoint = true
        fn()
        content.append(";\n")
    }

    override fun Point.unaryMinus() {
        if (firstPoint) {
            content.append("(${scale(x)}, ${scale(y)})")
        } else {
            content.append(" -- (${scale(x)}, ${scale(y)})")
        }
        firstPoint = false
    }

    override fun String.unaryPlus() {
        if (context == ContextType.Itemize) {
            content.append("\\item $this\n")
        } else {
            content.append(this + "\n")
        }
    }

    override fun Point.unaryPlus() {
        if (firstPoint) {
            content.append("(${scale(x)}, ${scale(y)})")
        } else {
            content.append(" -- ++(${scale(x)}, ${scale(y)})")
        }

        firstPoint = false
    }

    override fun node(text: String, at: Point, style: String?) {
        content.append("\\node")
        if (style != null) content.append("[$style]")
        content.append(" at (${scale(at.x)}, ${scale(at.y)}) {$text};\n")
    }

    override fun Cycle.unaryMinus() {
        content.append(" -- cycle")
    }

    override fun section(name: String) {
        content.append("\\section{$name}\n")
    }

    override fun itemize(fn: ItemizeContext.() -> Unit) {
        scoped("itemize", newContext = ContextType.Itemize) {
            fn()
        }
    }

    override fun subSection(name: String) {
        content.append("\\subsection{$name}\n")
    }

    override fun onSlides(slides: String, fn: LatexDocument.() -> Unit) {
        content.append("\\onslide<$slides>{\n")
        fn()
        content.append("}\n")
    }
    override fun only(slides: String, fn: LatexDocument.() -> Unit) {
        content.append("\\only<$slides>{\n")
        fn()
        content.append("}\n")
    }
}
