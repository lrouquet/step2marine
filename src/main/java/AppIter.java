//import org.chocosolver.solver.Solver;

import com.github.rloic.phd.core.mzn.*;
import com.github.rloic.phd.core.utils.ArgumentsKt;
import com.github.rloic.phd.core.utils.Logger;
import com.github.rloic.phd.core.utils.LoggerKt;
import com.github.rloic.phd.infra.MiniZincBinary;
import com.github.rloic.phd.infra.PicatBinary;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.variables.IntVar;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

import static java.lang.String.format;

/*
    La recherche est supprimée des fichiers mzn et est ajoutée automatiquement par le code java ci-dessous.
    Ça permet d'avoir le même code pour la step1 opt et la step1 enum.

    Le dossier mzn_models contient trois fichiers:
    - WARP_Step1_Boomerang.mzn : le fichier contenant la step 1 à prorpement parler
    - WARP_Step1_Boomerang.optimized.mzn : le fichier contenant la step 1 à prorpement parler avec améliorations
    - WARP_Step1_Boomerang_decision_vars.mzn : le fichier contenant les variables pour l'heuristique de recherche
    - WARP_Step1_Boomerang_forbid_solution.mzn : le fichier donnant la contraint interdisant la solution précédente
 */
public class AppIter {

    private static final Config config = new Config("config.txt");

    private static final PartialMznModel MODEL =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang.optimized.mzn"));
    private static final PartialMznModel DECISION_VARS =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_decision_vars.mzn"));
    private static final PartialMznModel FORBID_SOLUTION_CONSTRAINT =
            new PartialMznModel(new File("mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn"));

    /*
    Construit le modèle step 1 opt à partir du fichier MODEL
    La variable objective est "obj" et on cherche à minimizer.
    Le modèle complet est construit dans le fichier step1_opt.mzn
     */
    private static MznModel.Optimization STEP1_OPT() {
        return new MznModelBuilder.Optimization(
                Collections.singletonList(MODEL),
                new MznVariable("obj"),
                OptimizationSearch.Minimize
        ).build(new File("mzn_models/tmp/step1_iter.mzn"));
    }

    /*
    Construit le modèle step 1 enum à partir du fichier MODEL
    Les variables de décisions sont décrites dans le fichier DECIVION_VARS,
    la contrainte interdisant la solution prédécente est décrite dans le fichier FORBID_SOLUTION_CONSTRAINT
     */
    private static MznModel.PartialSearch STEP1_ENUM(int Nr) {
        return new MznModelBuilder.PartialAssignment(
                Collections.singletonList(MODEL),
                MznSearchConfiguration.Companion.from(
                        DECISION_VARS,
                        SearchStrategy.DomOverWDeg,
                        ValueSelector.InDomainMin
                ),
                FORBID_SOLUTION_CONSTRAINT,
                AppIter::getPartialAssignment
        ).build(new File(format("mzn_models/tmp/step1_enum_%d.mzn", Nr)));
    }

    private static final int[] WARP_SBOX = new int[]{
            0xc, 0xa, 0xd, 0x3, 0xe, 0xb, 0xf, 0x7, 0x8, 0x9, 0x1, 0x5, 0x0, 0x2, 0x4, 0x6
    };

    public static final SboxTables SBOXES = new SboxTables(new Sbox(WARP_SBOX), 1);

    // La fonction est utilisée pour supprimer les anciennes solutions dans la step 1.
    public static Assignment.Partial getPartialAssignment(MznSolution warpMznSoltion) {
        StringBuilder builder = new StringBuilder();

        String[] lines = warpMznSoltion.getContent().split("\n");

        boolean[] DXupper = getBoolArray(lines, "DXupper");
        builder.append("array [ROUNDS, BR] of var bool: DXupper_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(DXupper))
                .append(");\n");

        boolean[] FreeXupper = getBoolArray(lines, "FreeXupper");
        builder.append("array [ROUNDS, BR] of var bool: FreeXupper_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(FreeXupper))
                .append(");\n");

        boolean[] FreeSBupper = getBoolArray(lines, "FreeSBupper");
        builder.append("array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBupper_%SOL% = array2d(ATTACK_ROUNDS, BR_HALF, ")
                .append(Arrays.toString(FreeSBupper))
                .append(");\n");

        boolean[] DXlower = getBoolArray(lines, "DXlower");
        builder.append("array [ROUNDS, BR] of var bool: DXlower_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(DXlower))
                .append(");\n");

        boolean[] FreeXlower = getBoolArray(lines, "FreeXlower");
        builder.append("array [ROUNDS, BR] of var bool: FreeXlower_%SOL% = array2d(ROUNDS, BR, ")
                .append(Arrays.toString(FreeXlower))
                .append(");\n");

        boolean[] FreeSBlower = getBoolArray(lines, "FreeSBlower");
        builder.append("array [ATTACK_ROUNDS, BR_HALF] of var bool: FreeSBlower_%SOL% = array2d(ATTACK_ROUNDS, BR_HALF, ")
                .append(Arrays.toString(FreeSBlower))
                .append(");\n");

        return new Assignment.Partial(builder.toString());
    }

    public static void main(String[] _args_) throws IOException {
        createFolderTree();

        Map<String, String> args = ArgumentsKt.parseArgs(_args_, "=");

        LoggerKt.setLogger(Logger.Companion.from(args));
        LoggerKt.getLogger().addSubscriber(System.out, Logger.HeaderMode.NONE, false, null);
        BufferedWriter logWriter = new BufferedWriter(new FileWriter("logs/warp-step1-opt-iter.log"));
        LoggerKt.getLogger().addSubscriber(logWriter, Logger.HeaderMode.NONE, true, Logger.Level.ALL);

        MznSolver step1OptSolver;
        switch (args.getOrDefault("Step1Solver", null)) {
            case "Picat":
                MiniZincBinary minizinc =
                        new MiniZincBinary(args.getOrDefault("MiniZinc", config.get("MiniZinc")));

                step1OptSolver = new PicatBinary(
                        args.getOrDefault("Picat", config.get("Picat")),
                        args.getOrDefault("PicatFzn", config.get("PicatFzn")),
                        minizinc
                );
                LoggerKt.getLogger().log("Step1Solver = Picat");
                break;
            case "Gurobi":
                step1OptSolver = new GurobiBinary(
                        args.getOrDefault("MiniZinc", config.get("MiniZinc")),
                        Integer.parseInt(args.getOrDefault("NbThreads", "1"))
                );
                LoggerKt.getLogger().log("Step1Solver = Gurobi");
                break;
            default:
                LoggerKt.getLogger().error("Argument Step1Solver is empty or invalid. Valid solvers are : Picat or Gurobi.");
                return;
        }

        List<Integer> prevUB = new ArrayList<>();
        for (int Nr = 3; Nr < 41; Nr++) {
            LoggerKt.getLogger().info("Starting Step1Opt for Nr = " + Nr);
            Instant start = Instant.now();
            Integer objStep1 = runStep1Opt(Nr, prevUB, step1OptSolver);
            LoggerKt.getLogger().log("Time to solve Step1Opt for Nr = %d, %s", Nr, secondsElapsedSince(start));
            if (objStep1 == null) {
                LoggerKt.getLogger().warn("No solution was found");
                return;
            } else {
                LoggerKt.getLogger().log("ObjStep1 for Nr = %d, 2^{-%d}", Nr, objStep1);
                prevUB.add(objStep1);
            }
        }
    }

    private static boolean[] getBoolArray(String[] lines, String key) {
        for (String line : lines) {
            if (line.startsWith(key)) {
                return parseBooleanArray(line);
            }
        }
        return null;
    }

    private static boolean[] parseBooleanArray(String line) {
        String[] values = line.substring(line.indexOf('['), line.lastIndexOf(']'))
                .split(",");
        boolean[] result = new boolean[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = (values[i].equalsIgnoreCase("true") || values[i].equalsIgnoreCase("1"));
        }
        return result;
    }

    private static void createFolderTree() {
        new File("logs").mkdirs();
        new File("solutions/step1").mkdirs();
        new File("solutions/step2").mkdirs();
        new File("solutions/verif").mkdirs();
    }

    private static Integer runStep1Opt(int Nr, List<Integer> prevUB, MznSolver sovler) throws IOException {
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("Nr", Nr);
        MznModel.Optimization model = STEP1_OPT();
        try (BufferedWriter bf = new BufferedWriter(new FileWriter(model.getValue(), true))) {
            for (int i = 0; i < prevUB.size(); i++) {
                int r = i + 3;
                bf.append(String.format("constraint sum(round in 0..%d, col in BR_HALF) (isTable(round, col) + isDDT2(round, col)) >= %d;\n", r, prevUB.get(i) / 2));
            }
        } catch (IOException ioe) {}
        MznSolution step1OptSol = sovler.optimize(model, parameters);
        if (step1OptSol == null) return null;

        String[] lines = step1OptSol.getContent().split("\n");
        for (String line : lines) {
            if (line.startsWith("obj = ")) {
                return Integer.parseInt(line.substring("obj = ".length(), line.lastIndexOf(';')));
            }
        }

        throw new RuntimeException("Cannot parse obj from the model");
    }

    private static class Seconds {
        private final Duration duration;

        public Seconds(Duration duration) {
            this.duration = duration;
        }

        @Override
        public String toString() {
            return String.format("%.2fs", duration.toMillis() / 1000.0);
        }

        public static final Seconds ZERO = new Seconds(Duration.ZERO);

        public Seconds plus(Seconds other) {
            return new Seconds(duration.plus(other.duration));
        }

    }

    private static Seconds secondsElapsedSince(Instant start) {
        return new Seconds(Duration.between(start, Instant.now()));
    }

}
