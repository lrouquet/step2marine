import com.github.rloic.phd.core.utils.LoggerKt;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.variables.IntVar;

import java.util.ArrayList;
import java.util.List;

public class Step2 {

    // TOUT CHANGER EN DEUX D
    // shuffle à mettre ici
    // public static final int[] shuffle= {31,6,29,14,1,12,21,8,27,2,3,0,25,4,23,10,15,22,13,30,17,28,5,24,11,18,19,16,9,20,7,26};

    public static final int[] Pi_even = {31, 29, 1, 21, 27, 3, 25, 23, 15, 13, 17, 5, 11, 19, 9, 7};
    public static final int[] Pi_odd = {6, 14, 12, 8, 2, 0, 4, 10, 22, 30, 28, 24, 18, 16, 20, 26};

    public final Model model = new Model();
    private final IntVar FREE = model.intVar("FREE", -1);
    private final IntVar ZERO = model.intVar("ZERO", 0);
    private final Tuples XOR_TUPLES = XOR_TUPLES();

    public final IntVar[][] δXupper;
    public final IntVar[][] δSXupper;

    public final IntVar[][] δXlower;
    public final IntVar[][] δSXlower;

    public final IntVar[][] proba;
    public final IntVar objective;

    private final int BR_HALF = 16;

    public Step2(
            int Nr,
            SboxTables SBOX_TABLES,
            int LB,
            int[][] DXupper, int[][] freeXupper, int[][] freeSBupper,
            int[][] DXlower, int[][] freeXlower, int[][] freeSBlower
    ) {

        δXupper = byteVars(Nr + 1, DXupper, freeXupper); //deltaXupper recoit DXupper et freeXupper
        δSXupper = byteVars2(Nr, DXupper, freeSBupper); //deltaXupper recoit DXupper et freeSBupper

        δXlower = byteVars(Nr + 1, DXlower, freeXlower);
        δSXlower = byteVars2(Nr, DXlower, freeSBlower);

        proba = new IntVar[Nr][16];

        int nbSb = 0;
        Bounds objectiveBounds = new Bounds(0, 0);
        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < BR_HALF; j++) {
                IntVar Delta_i = δXupper[i][2 * j];
                IntVar delta = δSXupper[i][j];
                IntVar Nabla_o = δXlower[i][2 * j];
                IntVar alpha = δSXlower[i][j];

                if (BoomerangRules.isDDTupper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationDDTBounds);
                    model.table(new IntVar[]{Delta_i, delta, proba[i][j]}, SBOX_TABLES.relationDDT).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationDDTBounds);
                    nbSb += 1;
                } else if (BoomerangRules.isDDTlower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationDDTBounds);
                    model.table(new IntVar[]{Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationDDT).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationDDTBounds);
                    nbSb += 1;
                } else if (BoomerangRules.isDDT2upper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationDDT2Bounds);
                    model.table(new IntVar[]{Delta_i, delta, proba[i][j]}, SBOX_TABLES.relationDDT2).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationDDT2Bounds);
                    nbSb += 1;
                } else if (BoomerangRules.isDDT2lower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationDDT2Bounds);
                    model.table(new IntVar[]{Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationDDT2).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationDDT2Bounds);
                    nbSb += 1;
                } else if (BoomerangRules.isFBCT(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationFBCTBounds);
                    model.table(new IntVar[]{Delta_i, Nabla_o, proba[i][j]}, SBOX_TABLES.relationFBCT).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationFBCTBounds);
                    nbSb += 1;
                } else if (BoomerangRules.isFBDTUpper(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationFBDTBounds);
                    model.table(new IntVar[]{Delta_i, delta, Nabla_o, proba[i][j]}, SBOX_TABLES.relationFBDT).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationFBDTBounds);
                    nbSb += 1;
                } else if (BoomerangRules.isFBDTLower(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationFBDTBounds);
                    model.table(new IntVar[]{Nabla_o, alpha, Delta_i, proba[i][j]}, SBOX_TABLES.relationFBDT).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationFBDTBounds);
                    nbSb += 1;
                } else if (BoomerangRules.isFBET(DXupper[i][2 * j], freeXupper[i][2 * j], freeSBupper[i][j], DXlower[i][2 * j], freeXlower[i][2 * j], freeSBlower[i][j])) {
                    proba[i][j] = intVar(SBOX_TABLES.relationFBETBounds);
                    model.table(new IntVar[]{Delta_i, delta, Nabla_o, alpha, proba[i][j]}, SBOX_TABLES.relationFBET).post();
                    objectiveBounds = objectiveBounds.plus(SBOX_TABLES.relationFBETBounds);
                    nbSb += 1;
                }
            }
        }

        LoggerKt.getLogger().info("Number of active S-Boxes: %d", nbSb);

        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < BR_HALF; j++) {
                δXupper[i + 1][Pi_even[j]].eq(δXupper[i][2 * j]).post();
                δXlower[i + 1][Pi_even[j]].eq(δXlower[i][2 * j]).post();

                postXor(δXupper[i + 1][Pi_odd[j]], δXupper[i][2 * j + 1], δSXupper[i][j]);
                postXor(δXlower[i + 1][Pi_odd[j]], δXlower[i][2 * j + 1], δSXlower[i][j]);
            }
        }

        if (LB < objectiveBounds.max) {
            if (LB <= objectiveBounds.min) {
                model.arithm(model.boolVar(true), "=", model.boolVar(false)).post();
            } else {
                objectiveBounds = new Bounds(objectiveBounds.min, LB);
            }
        }
        objective = intVar(objectiveBounds);

        // 2D
        List<IntVar> activeSboxes = new ArrayList<>();
        for (int i = 0; i < Nr; i++) {
            for (int j = 0; j < 16; j++) {
                if (proba[i][j] != null) {
                    activeSboxes.add(proba[i][j]);
                }
            }
        }

        IntVar[] myArray = new IntVar[activeSboxes.size()];
        for (int i = 0; i < activeSboxes.size(); i++) {
            myArray[i] = activeSboxes.get(i);
        }

        model.sum(myArray, "=", objective).post();
        model.setObjective(Model.MINIMIZE, objective);
    }

    private IntVar intVar(Bounds bounds) {
        return model.intVar(bounds.min, bounds.max);
    }

    // Affectation de l'état => passage à 2 D
    // donc deltastate porte une troisième dimension contenant si il est libre ou non
    // en sortie en fait, on a une variable qui vaut diff(0), ZERO ou FREE
    // A MODIFIER PB DU 2*I pas de la même longueur
    // TABLEAU 32
    private IntVar[][] byteVars(int Nr, int[][] Δstate, int[][] freeState) {
        IntVar[][] δstate = new IntVar[Δstate.length][];
        for (int i = 0; i < Nr; i++) {
            δstate[i] = new IntVar[Δstate[i].length];
            for (int j = 0; j < Δstate[i].length; j++) {
                // δstate[i][j] = new IntVar[Δstate[i][j].length];
                // for (int k = 0; k < Δstate[i][j].length; k++) {
                if (freeState[i][j] == 0) {
                    if (Δstate[i][j] == 1) {
                        δstate[i][j] = model.intVar(1, 15);
                    } else {
                        δstate[i][j] = ZERO;
                    }
                } else {
                    δstate[i][j] = FREE;
                }
            }
        }

        return δstate;
    }

    // TABLEAU 16
    private IntVar[][] byteVars2(int Nr, int[][] Δstate, int[][] freeSBState) {
        IntVar[][] δstate = new IntVar[Δstate.length][];
        for (int i = 0; i < Nr; i++) {
            δstate[i] = new IntVar[Δstate[i].length / 2];
            for (int j = 0; j < Δstate[i].length / 2; j++) {
                // δstate[i][j] = new IntVar[Δstate[i][j].length];
                // for (int k = 0; k < Δstate[i][j].length; k++) {
                if (freeSBState[i][j] == 0) { // ATTENTION ICI C'est forcement FreeSB qui est appelé
                    if (Δstate[i][2 * j] == 1) {
                        δstate[i][j] = model.intVar(1, 15);
                    } else {
                        δstate[i][j] = ZERO;
                    }
                } else {
                    δstate[i][j] = FREE;
                }
            }
        }

        return δstate;
    }

    private void postXor(IntVar a, IntVar b, IntVar c) {
        if (a == FREE || b == FREE || c == FREE) return;

        if (a == ZERO && b == ZERO && c == ZERO) return;

        if (a == ZERO && b == ZERO) {
            model.arithm(c, "=", ZERO).post();
            return;
        }

        if (b == ZERO && c == ZERO) {
            model.arithm(a, "=", ZERO).post();
            return;
        }

        if (a == ZERO && c == ZERO) {
            model.arithm(b, "=", ZERO).post();
            return;
        }

        if (a == ZERO) {
            model.arithm(b, "=", c).post();
            return;
        }

        if (b == ZERO) {
            model.arithm(c, "=", a).post();
            return;
        }

        if (c == ZERO) {
            model.arithm(a, "=", b).post();
            return;
        }

        if (a.getLB() > 0) {
            model.arithm(b, "!=", c).post();
        }

        if (b.getLB() > 0) {
            model.arithm(a, "!=", c).post();
        }

        if (c.getLB() > 0) {
            model.arithm(b, "!=", a).post();
        }

        model.table(new IntVar[]{a, b, c}, XOR_TUPLES).post();
    }

    // changement taille
    private Tuples XOR_TUPLES() {
        Tuples tuples = new Tuples();

        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                tuples.add(x, y, x ^ y);
            }
        }

        return tuples;
    }

}
