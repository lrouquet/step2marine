
// cela c'est OK, j'ai compris, c'est toutes les conditions de rentrée dans les tables
public class BoomerangRules {

    private static boolean isActive(Integer DX) {
        return DX == 1;
    }
    private static boolean isZero(Integer DX) {
        return DX == 0;
    }
    private static boolean isFree(Integer freeVar) { return freeVar == 1; }
    private static boolean isFixed(Integer freeVar) {
        return freeVar == 0;
    }

    public static boolean isDDT(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isDDTupper(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower)
                || isDDTlower(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower);
    }

    public static boolean isDDTupper(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isActive(DXupper) && isFixed(freeXupper) && isFixed(freeSBupper) && isZero(DXlower);
    }

    public static boolean isDDTlower(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isZero(DXupper) && isActive(DXlower) && isFixed(freeXlower) && isFixed(freeSBlower);
    }

    public static boolean isDDT2(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isDDT2upper(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower)
                || isDDT2lower(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower);
    }

    public static boolean isDDT2upper(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isActive(DXupper) && isFixed(freeXupper) && isFixed(freeSBupper) && isActive(DXlower) && isFree(freeXlower) && isFree(freeSBlower);
    }

    public static boolean isDDT2lower(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isActive(DXupper) && isFree(freeXupper) && isFree(freeSBupper) && isActive(DXlower) && isFixed(freeXlower) && isFixed(freeSBlower);
    }

    public static boolean isFBDT(
            Integer DXupper, Integer freeXupper, Integer freeSBupper,
            Integer DXlower, Integer freeXlower, Integer freeSBlower
    ) {
        return isFBDTUpper(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower) ||
                isFBDTLower(DXupper, freeXupper, freeSBupper, DXlower, freeXlower, freeSBlower);
    }

    public static boolean isFBDTUpper(
            Integer DXupper, Integer delta_i, Integer delta,
            Integer DXlower, Integer nabla_o, Integer alpha
    ) {
        return isActive(DXupper) && isFixed(delta_i) && isFixed(delta) && isActive(DXlower) && isFixed(nabla_o) && isFree(alpha);
    }

    public static boolean isFBDTLower(
            Integer DXupper, Integer delta_i, Integer delta,
            Integer DXlower, Integer nabla_o, Integer alpha
    ) {
        return isActive(DXupper) && isFixed(delta_i) && isFree(delta) && isActive(DXlower) && isFixed(nabla_o) && isFixed(alpha);
    }

    public static boolean isFBCT(
            Integer DXupper, Integer delta_i, Integer delta,
            Integer DXlower, Integer nabla_o, Integer alpha
    ) {
        return isActive(DXupper) && isFixed(delta_i) && isFree(delta) && isActive(DXlower) && isFixed(nabla_o) && isFree(alpha);
    }

    public static boolean isFBET(
            Integer DXupper, Integer delta_i, Integer delta,
            Integer DXlower, Integer nabla_o, Integer alpha
    ) {
        return isActive(DXupper) && isFixed(delta_i) && isFixed(delta) && isActive(DXlower) && isFixed(nabla_o) && isFixed(alpha);
    }

}
