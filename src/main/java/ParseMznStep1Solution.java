import com.github.rloic.phd.core.mzn.MznSolution;

import java.io.*;
import java.util.Arrays;
import java.util.stream.Stream;

public class ParseMznStep1Solution {

    private final MznSolution step1Sol;

    public ParseMznStep1Solution(MznSolution step1Sol) {
        this.step1Sol = step1Sol;
    }

    // voir la tête du fichier d'entrée sinon, enlever les Nb => tout passer en 2D à 32 => Nr, 32
    // OK, je le fais maintenant et je regarderai si cela marhce
    public Step1Solution parseSolution(int Nr) throws IOException {

        int[][] DXupper = null;
        int[][] freeXupper = null;
        int[][] freeSBupper = null;
        int[][] DXlower = null;
        int[][] freeXlower = null;
        int[][] freeSBlower = null;

        String[] lines = step1Sol.getContent().split("\n");
        for (String line : lines) {
            if (line.startsWith("DXupper")) {
                DXupper = reshape(extract(line), Nr + 1, 32);
            }

            if (line.startsWith("FreeXupper")) {
                freeXupper = reshape(extract(line), Nr + 1, 32);
            }

            if (line.startsWith("FreeSBupper")) {
                freeSBupper = reshape(extract(line), Nr, 16);
            }

            if (line.startsWith("DXlower")) {
                DXlower = reshape(extract(line), Nr + 1, 32);
            }

            if (line.startsWith("FreeXlower")) {
                freeXlower = reshape(extract(line), Nr + 1, 32);
            }

            if (line.startsWith("FreeSBlower")) {
                freeSBlower = reshape(extract(line), Nr, 16);
            }
        }

        assert DXupper != null && freeXupper != null && freeSBupper != null && DXlower != null && freeXlower != null && freeSBlower != null;

        return new Step1Solution(
                DXupper, freeXupper, freeSBupper,
                DXlower, freeXlower, freeSBlower
        );
    }

    private static int[][] reshape(int[] array, int dim1, int dim2) {
        assert dim1 * dim2 == array.length;
        int[][] tensor = new int[dim1][dim2];

        int cpt = 0;
        for (int i = 0; i < dim1; i++) {
            for (int j = 0; j < dim2; j++) {
                tensor[i][j] = array[cpt++];
            }
        }

        return tensor;
    }

    private static int[] extract(String line) {
        return Arrays.stream(line.substring(line.indexOf("[") + 1, line.lastIndexOf(']')).split(", ?"))
                .mapToInt(it -> {
                    assert isBoolean(it);
                    return (it.trim().equalsIgnoreCase("1") || it.trim().equalsIgnoreCase("true")) ? 1 : 0;
                })
                .toArray();
    }

    private static boolean isBoolean(String s) {
        return Stream.of("1", "0", "true", "false").anyMatch(it -> it.equalsIgnoreCase(s));
    }

}
