import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import org.chocosolver.solver.constraints.extension.Tuples;

public class SboxTables {

    static class BCCTables {
        public final int[][] fbct;
        public final Object2IntMap<FBDTArgs> fbdt;
        public final Object2IntMap<FBETArgs> fbet;

        public BCCTables(int[][] fbct, Object2IntMap<FBDTArgs> fbdt, Object2IntMap<FBETArgs> fbet) {
            this.fbct = fbct;
            this.fbdt = fbdt;
            this.fbet = fbet;
        }
    }

    private final Sbox S;
    private final int MAX_VALUE;
    private final int[] VALUES;
    private final int[] NON_ZERO_VALUES;
    private final int[] PROBA_EXPONENTS;

    private final int[][] DDT;
    private final int[][] FBCT;
    private final Object2IntMap<FBDTArgs> FBDT;
    private final Object2IntMap<FBETArgs> FBET;

    public final Tuples relationDDT;
    public final Bounds relationDDTBounds;

    public final Tuples relationDDT2;
    public final Bounds relationDDT2Bounds;

    public final Tuples relationFBCT;
    public final Bounds relationFBCTBounds;

    public final Tuples relationFBDT;
    public final Bounds relationFBDTBounds;

    public final Tuples relationFBET;
    public final Bounds relationFBETBounds;

    public SboxTables(Sbox S) {
        this(S, 1);
    }

    public SboxTables(Sbox S, int probaFactor) {
        this.S = S;
        this.MAX_VALUE = S.len();
        this.VALUES = new int[MAX_VALUE];
        this.NON_ZERO_VALUES = new int[MAX_VALUE - 1];
        VALUES[0] = 0;
        for (int x = 1; x < MAX_VALUE; x++) {
            VALUES[x] = x;
            NON_ZERO_VALUES[x - 1] = x;
        }
        this.DDT = generateDDT();

        BCCTables tables = generateFBCTs();
        this.FBCT = tables.fbct;
        this.FBDT = tables.fbdt;
        this.FBET = tables.fbet;

        this.PROBA_EXPONENTS = new int[MAX_VALUE + 1];
        for (int i = 0; i <= MAX_VALUE; i++) {
            PROBA_EXPONENTS[i] = (int) (- probaFactor * log2((double) i / (double) S.len()));
        }

        relationDDT = createRelationDDT(1);
        relationDDTBounds = probabilityBounds(relationDDT);
        relationDDT2 = createRelationDDT(2);
        relationDDT2Bounds = probabilityBounds(relationDDT2);
        relationFBCT = createRelationFBCT();
        relationFBCTBounds = probabilityBounds(relationFBCT);
        relationFBDT = createRelationFBDT();
        relationFBDTBounds = probabilityBounds(relationFBDT);
        relationFBET = createRelationFBET();
        relationFBETBounds = probabilityBounds(relationFBET);
    }

    private double log2(double n) {
        return Math.log(n) / Math.log(2.0);
    }

    private int[][] generateDDT() {
        int[][] ddt = new int[MAX_VALUE][MAX_VALUE];

        for (int x : NON_ZERO_VALUES) {
            for (int Δi : NON_ZERO_VALUES) {
                int δ = S.get(x) ^ S.get(x ^ Δi);
                ddt[Δi][δ] += 1;
            }
        }

        return ddt;
    }

    // j'en suis là il faut que je réfléchisse...
    // A CHANGER EN FBCT, a changer aussi pour lambda car pas d'appel à S^-1 => voir en détails
    private BCCTables generateFBCTs() {

        int[][] fbct = new int[MAX_VALUE][MAX_VALUE];
        Object2IntMap<FBDTArgs> fbdt = new Object2IntOpenHashMap<>();
        Object2IntMap<FBETArgs> fbet = new Object2IntOpenHashMap<>();

        for (int x : VALUES) {
            for (int Δi : VALUES) {
                for (int Δo : VALUES) {
                    if ((S.get(x) ^ S.get(x ^ Δi) ^ S.get(x ^ Δo) ^ S.get(x ^ Δi ^ Δo)) == 0) {
                        fbct[Δi][Δo] += 1;
                        int δ = S.get(x) ^ S.get(x ^ Δi);
                        int α = S.get(x ^ Δi) ^ S.get(x ^ Δi ^ Δo);
                        increaseByOne(fbdt, new FBDTArgs(Δi, δ, Δo));
                        increaseByOne(fbet, new FBETArgs(Δi, δ, Δo, α));
                    }
                }
            }
        }

        return new BCCTables(fbct, fbdt, fbet);

    }

    private <K> void increaseByOne(Object2IntMap<K> map, K key) {
        int current;
        if (map.containsKey(key)) {
            current = map.getInt(key);
        } else {
            current = 0;
        }
        map.put(key, current + 1);
    }

    private Tuples createRelationDDT(int factor) {
        Tuples tuples = new Tuples();

        for (int gamma : NON_ZERO_VALUES) {
            for (int delta : NON_ZERO_VALUES) {
                if (DDT[gamma][delta] != 0) {
                    tuples.add(gamma, delta, factor * PROBA_EXPONENTS[DDT[gamma][delta]]);
                }
            }
        }

        return tuples;
    }

    // A MODIFIER SELONE LES ENTREES SORTIES
    private Tuples createRelationFBCT() {
        Tuples tuples = new Tuples();

        for (int Δi : NON_ZERO_VALUES) {
            for (int Δo : NON_ZERO_VALUES) {
                if (FBCT[Δi][Δo] != 0) {
                    tuples.add(Δi, Δo, PROBA_EXPONENTS[FBCT[Δi][Δo]]);
                }
            }
        }

        return tuples;
    }

    // A MODIFIER SELONE LES ENTREES SORTIES
    private Tuples createRelationFBDT() {
        Tuples tuples = new Tuples();

        for (Object2IntMap.Entry<FBDTArgs> entry : FBDT.object2IntEntrySet()) {
            if (entry.getKey().Δi != 0 && entry.getKey().δ != 0 && entry.getKey().Δo != 0) {
                addKeyValueInTable(tuples, entry.getKey(), entry.getIntValue());
            }
        }

        return tuples;
    }

    // A MODIFIER SELON ENTREE/SORTIE
    private Tuples createRelationFBET() {
        Tuples tuples = new Tuples();

        for (Object2IntMap.Entry<FBETArgs> entry : FBET.object2IntEntrySet()) {
            if (entry.getKey().Δi != 0 && entry.getKey().δ != 0 && entry.getKey().Δo != 0 && entry.getKey().α != 0) {
                addKeyValueInTable(tuples, entry.getKey(), entry.getIntValue());
            }
        }

        return tuples;
    }


    // A MODIFIER SELON ENTREE/SORTIE
    private void addKeyValueInTable(Tuples table, FBDTArgs key, int value) {
        table.add(key.Δi, key.δ, key.Δo, PROBA_EXPONENTS[value]);
    }

    // A MODIFIER SELON ENTREE/SORTIE
    private void addKeyValueInTable(Tuples table, FBETArgs key, int value) {
        table.add(key.Δi, key.δ, key.Δo, key.α, PROBA_EXPONENTS[value]);
    }

    // OK
    private Bounds probabilityBounds(Tuples table) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for (int i = 0; i < table.nbTuples(); i++) {
            int[] tuple = table.get(i);
            int probability = tuple[tuple.length - 1];
            if (probability < min) min = probability;
            if (probability > max) max = probability;
        }

        return new Bounds(min, max);
    }
}
