public class Step1Solution {

    public final int[][] ΔXupper;
    public final int[][] freeXupper;
    public final int[][] freeSXupper;


    public final int[][] ΔXlower;
    public final int[][] freeXlower;
    public final int[][] freeSXlower;

    public final BoomerangTable[][] tables;

    public Step1Solution(int[][] ΔXupper, int[][] freeXupper, int[][] freeSXupper, int[][] ΔXlower, int[][] freeXlower, int[][] freeSXlower) {
        this.ΔXupper = ΔXupper;
        this.freeXupper = freeXupper;
        this.freeSXupper = freeSXupper;
        this.ΔXlower = ΔXlower;
        this.freeXlower = freeXlower;
        this.freeSXlower = freeSXlower;

        this.tables = new BoomerangTable[ΔXupper.length - 1][ΔXupper[0].length / 2];
        for (int i = 0; i < ΔXupper.length - 1; i++) {
            for (int j = 0; j < ΔXupper[i].length / 2; j++) {
                if (BoomerangRules.isDDTupper(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.DDT;
                } else if (BoomerangRules.isDDTlower(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.DDT;
                } else if (BoomerangRules.isDDT2upper(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.DDT2;
                } else if (BoomerangRules.isDDT2lower(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.DDT2;
                } else if (BoomerangRules.isFBCT(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.FBCT;
                } else if (BoomerangRules.isFBDTUpper(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.FBDT;
                } else if (BoomerangRules.isFBDTLower(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.FBDT;
                } else if (BoomerangRules.isFBET(ΔXupper[i][2 * j], freeXupper[i][2 * j], freeSXupper[i][j], ΔXlower[i][2 * j], freeXlower[i][2 * j], freeSXlower[i][j])) {
                    tables[i][j] = BoomerangTable.FBET;
                }
            }
        }
    }
}
