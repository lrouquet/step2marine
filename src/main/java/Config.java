import java.io.*;
import java.util.HashMap;

public class Config {

    private final HashMap<String, String> parameters = new HashMap<>();

    public Config(String path) {
        File configFile = new File(path);
        try (FileReader fr = new FileReader(configFile)){
            BufferedReader bufReader = new BufferedReader(fr);
            String line;
            while ((line = bufReader.readLine()) != null) {
                if (!line.startsWith("#")) {
                    String[] parts = line.split(" ?= ?");
                    if (parts.length == 2) {
                        String key = parts[0];
                        String value = parts[1];

                        parameters.put(key, value);
                    }
                }
            }
        } catch (FileNotFoundException fe) {
            System.err.println("Cannot find the configuration file " + path + ".");
            System.err.println("The configuration will be empty.");
        } catch (IOException ioe) {
            throw new RuntimeException("An error occurs when reading the configuration file " + path, ioe);
        }
    }

    public String get(String key) {
        return parameters.get(key);
    }

}
