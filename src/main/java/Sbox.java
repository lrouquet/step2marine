public class Sbox {

    private final int[] sbox;
    private final int[] sboxInv;

    public Sbox(int[] sbox) {
        this.sbox = sbox;
        this.sboxInv = new int[sbox.length];
        for(int x = 0; x < sbox.length; x++) {
            sboxInv[sbox[x]] = x;
        }
    }

    public int get(int n) {
        return sbox[n];
    }

    public int inv(int n) {
        return sboxInv[n];
    }

    public int len() {
        return sbox.length;
    }

}
