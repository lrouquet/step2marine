import java.util.Objects;

public class FBDTArgs {

    public final int Δi;
    public final int δ;
    public final int Δo;

    public FBDTArgs(int Δi, int δ, int Δo) {
        this.Δi = Δi;
        this.δ = δ;
        this.Δo = Δo;
    }

    @Override
    public String toString() {
        return "FBDTArgs{" +
                "Din=" + Δi +
                ", delta=" + δ +
                ", Dout=" + Δo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FBDTArgs fbdtArgs = (FBDTArgs) o;
        return Δi == fbdtArgs.Δi && δ == fbdtArgs.δ && Δo == fbdtArgs.Δo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Δi, δ, Δo);
    }

}
