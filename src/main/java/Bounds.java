import java.util.Objects;

/*
Cette classe permet de représenter les bornes des probabilités pour les différentes tables.
Le this.quelquechose permet d'enregistrer la variable à l'initialisation
/!\ Les bounds sont écrits en -log2 des probabilités ce qui veut dire que new Bounds(6, 7) représente l'intervalle
    [2^{-7}, 2^{-6}]
 */
public class Bounds {

    public final int min;
    public final int max;

    public Bounds(int min, int max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bounds bounds = (Bounds) o;
        return min == bounds.min && max == bounds.max;
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return "Bounds{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }

    public Bounds plus(Bounds other) {
        return new Bounds(min + other.min, max + other.max);
    }

}
