import java.util.Objects;

public class UBCTArgs {

    public final int gamma;
    public final int theta;
    public final int delta;

    public UBCTArgs(int gamma, int theta, int delta) {
        this.gamma = gamma;
        this.theta = theta;
        this.delta = delta;
    }

    @Override
    public String toString() {
        return "UBCTArgs{" +
                "gamma=" + gamma +
                ", theta=" + theta +
                ", delta=" + delta +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UBCTArgs ubctArgs = (UBCTArgs) o;
        return gamma == ubctArgs.gamma && theta == ubctArgs.theta && delta == ubctArgs.delta;
    }

    @Override
    public int hashCode() {
        return Objects.hash(gamma, theta, delta);
    }

}
