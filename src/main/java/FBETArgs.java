import java.util.Objects;

// toutes les régles de EBCT
public class FBETArgs {

    public final int Δi;
    public final int δ;
    public final int α;
    public final int Δo;

    public FBETArgs(int Δi, int δ, int Δo, int α) {
        this.Δi = Δi;
        this.δ = δ;
        this.Δo = Δo;
        this.α = α;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FBETArgs fbetArgs = (FBETArgs) o;
        return Δi == fbetArgs.Δi && δ == fbetArgs.δ && Δo == fbetArgs.Δo && α == fbetArgs.α;
    }

    @Override
    public String toString() {
        return "FBETArgs{" +
                "Din=" + Δi +
                ", delta=" + δ +
                ", Dout=" + Δo +
                ", alpha=" + α +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(Δi, δ, Δo, α);
    }
}
