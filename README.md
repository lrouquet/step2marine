# Feistel Boomerang

## Commandes à exécuter

```shell
git clone https://github.com/rloic/PhD-Core modules/phd-core
git clone https://github.com/rloic/PhD-Infrastructure modules/phd-infrastructure
```

## Step 1

L'étape 1 est décrite dans **3** fichiers : 
- `mzn_models/WARP_Step1_Boomerang.fzn`
- `mzn_models/WARP_Step1_Boomerang_decision_vars.mzn`
- `mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn`

### `mzn_models/WARP_Step1_Boomerang.fzn`

Ce fichier contient le modèle représentant les boomerangs. Il **ne** contient **pas** la commande `solve minimize obj;` 
qui est injectée automatiquement par la librairie `java` (ce qui permet de faire step 1 opt et step 1 enum avec le même modèle).

### `mzn_models/WARP_Step1_Boomerang_decision_vars.mzn`

Ce fichier contient la liste des variables de décisions de la step 1.

### `mzn_models/WARP_Step1_Boomerang_forbid_solution.mzn`

Ce fichier contient la/les contrainte(s) permettant d'interdire la dernière solution trouvée. Dans notre cas, on interdira
la même combinaison de DXupper, FreeXupper, FreeSXupper, DXlower, FreeXlower et FreeSXlower.

## Exécution

```shell
./gradlew.bat jar
java -cp build/libs/step2marine-1.0-SNAPSHOT.jar App 7
```